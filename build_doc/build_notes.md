## Alan and Friends build notes
**Please read it entirely first, it could be useful.**

## Building advices
I'd recommend soldering the connectors between the two boards first. Place the connectors, tighten the screws to hold it into place, and then solder each side.
Clean it if you need, while there is no component in this side.
Then, you may proceed to the classic plan: small components first (such as didodes, resistors, etc); then capacitors, DIP sockets, etc.

For the LEDs, I'd recommend leaving them until the end. Then, when the lenses are placed on the panel, stuff the LEDs on the PCB without soldering them, then push them in the lenses. When it is held inside, you may solder them.

When starting the module for the first time, check voltages at ICs supplies first.
When all ICs are in place, look for various signs of life (when clocked, LEDs light up, etc). When testing the CV outputs first, make sure to have all pots mid-range (if fully CCW the voltage won't be varying).

### Additional hardware
You may need 4 M3 screws with 10mm plastic spacers. M3 washers may be handy too, I use plastic (nylon) ones to adjust the height depending on the connectors used between the boards.

## Circuitry notes
The Turing Machine circuitry remains, for the most part, unchanged. Some values were tweaked and output ranges adapted for Serge specs, and some changes were made to make it more robust: clock input, noise circuit, etc.

As always, feel free to adapt RLED values to find the brightness you like. There are many LEDs on this module, so higher (than usual) RLED value is better in the long run. For the LEDs given in the BOM, I used 10K for RLED (those are low-current LEDs).

While untested, it _should_ work on +/-15V (since CMOS can take it). You'll need to adapt output values, especially the pulses buffers.

#### Clock input
The shift registers (CD4015) may behave oddly when the clock is taken directly from the input clock shaping circuitry (using an TL072), especially when the input clock is not sharp enough. For some reason, the TL07x has some problems to drive the shift registers properly. To make it more robust, the clock is buffered (CD4050) then used to drive the CD4015. This is not a problem to use one buffer for multiple inputs, since one CMOS output can drive many inputs.

#### Noise source
The noise source output, on the front panel, is slightly increased to reach around +/-5V. For compensate for this, R52 has been increased to 68K (original value is 47K). This allows the trimmer to be set for higher gain, but still allowing the Machine to lock properly. This trimmer value has been changed too, since we don't need the whole 1M range;  500K makes the setting easier.

#### Gates
In this version, the CD4050s directly drive the front LEDs, and are also used for the sequence loop (the original looped directly the shift registers into the sequence input).

#### Pulses outputs
Serge use 0-5V for triggers, so transistor buffers have been added. They should also protect the CD4081 outputs. If you wish to use a different range (some may need Eurorack's 0-8V), increasing the resistor tied to the emitter should work. Also, I included the LEDs only for combined outputs: it would be too flashy for me otherwise.

#### Volts output
With the given resistor value, the output range is around 0-10V. To reach a different range, change R37.

#### Main CV output
With current values, the output voltage reaches 10V. To change this, R47 and R48 may be changed. Here are different sets of value:

| R47 | R48 | Output voltage |
|-----|-----|---------------:|
| 1.6K| 1.6K|             ~8V|
| 1.3K|~1.3K|            ~10V|
| 2.5K|~2.5K|             ~5V|


#### CD4050 supply
In the original schematic, the CD4050 are powered with 9V regulators. In my builds, I didn't solder theses and instead bypassed the footprint with a small wire. They are present on the board anyway, if you ever want to use them (it should work, while untested). You might need to tweak the output buffer values, since you'll get a different range.

### 1+2+4+7 output
To respect the Serge grid, I choose not to include the 1+2+4+7 pulse output. However, the output is present on the second board, if you need it. It is *unbuffered and unprotected*, so take care if you decide to use it.


## Components
#### Capacitors
Ceramics C0G caps may be used almost everywhere, as for decoupling, I like X7R 100nF.

The pitch is always 5mm. Anyway, you can experiment with different capacitor types, since there is nothing audio or critical here.

#### Resistors
Standard metal-film, 1/4W, 1% precision are fine. I like Xicon because the blue is nice, but this probably does not matter.

#### Diodes
1N5817 will work, but 1N5818 have an higher DC blocking voltage (30V rather than 20V). It makes sense to use a rating adapted to the maximum voltage available.
For LEDs, it is better to use low-current ones, since there are many of them.

#### Headers for expanders
You'll need 2x8 2.54 connectors to connect any expander. These are not included in the BOM. I didn't test this feature, but it should work without any problem.
